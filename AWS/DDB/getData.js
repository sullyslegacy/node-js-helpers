var express = require('express');
var app = express();

var AWS = require('aws-sdk');
AWS.config.update({
	region: "us-east-1"
});
var dynamodb = new AWS.DynamoDB.DocumentClient();
   
 
const ddbTable = 'DynamoDBTableName'; 

//http://localhost:3000/getData
app.get('/getData', function(req, res){	
	var customerEmail = "email@domain.com";
	var params = {
		TableName: ddbTable,
		Key: {
			"emailAddress": customerEmail
		}
	};
	
	dynamodb.get(params, function(err, data) {
		if (err){
			console.log(err, err.stack);
			res.send(err);
			return;
		} 
		//console.log(data);
		res.send(data);
	});
});

app.listen(3000);
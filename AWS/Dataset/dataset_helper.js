/*
 * AWS Setup
 */
var AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';
var cognitosync = new AWS.CognitoSync();


//Variables
var COGNITO_IDENTITY_POOL_ID = "us-east-1:POOOOOOOOL_ID";
var COGNITO_DATASET_NAME = "userinfo";


/**
 * @param {string} identityId
 */
exports.getDataSet = function(identityId){
    return new Promise(function (resolve, reject){
        cognitosync.listRecords({
            DatasetName: COGNITO_DATASET_NAME, 
            IdentityId: identityId, 
            IdentityPoolId: COGNITO_IDENTITY_POOL_ID
        }, 
        function(err, data) {
            if (err){
                reject(err);
            } 
            else {
                //Retrieve dataset metadata and SyncSessionToken for subsequent calls
                //let  COGNITO_SYNC_TOKEN = data.SyncSessionToken;
                //let COGNITO_SYNC_COUNT = data.DatasetSyncCount;
                //console.log(COGNITO_SYNC_COUNT);
                //console.log(JSON.stringify(data));
                resolve(data);
            }
        });
    });
}

var express = require('express');
var app = express();

var AWS = require('aws-sdk');
var s3 = new AWS.S3();
AWS.config.setPromisesDependency(require('bluebird'));
AWS.config.update({
   region: "us-east-1"
});
   
 
const bucketName = 'some-s3-bucket'; 
const fileName = 'test.txt';

//http://localhost:3000/getData
app.get('/getData', function(req, res){
	var getParams = {
		Bucket: bucketName, 		// your bucket name,
		Key: fileName 				// path to the object you're looking for
	}

	s3.getObject(getParams, function(err, data) {
		if (err){
			console.log(JSON.stringify(err, null, 2));
			res.send(err);
			return;
		}
		var data = data.Body.toString('utf-8');
		res.send(data);
	});
});


app.get('/pushTestData', function(req, res){
	params = {
		Bucket: bucketName, 
		Key: fileName, 
		Body: 'Test Data'
	}
	s3.putObject(params, function(err, data) {
         if (err) {
             console.log(err);
			 res.send(err);
			 return;
         } 
		var message = "Successfully uploaded data.";
		console.log(message);
		res.send(message);
	});
});


app.post('/pushData', function(req, res){
	params = {
		Bucket: bucketName, 
		Key: fileName, 
		Body: 'Test Data'
	}
	s3.putObject(params, function(err, data) {
         if (err) {
             console.log(err);
			 res.send(err);
			 return;
         } 
		var message = "Successfully uploaded data.";
		console.log(message);
		res.send(message);
	});
});

app.listen(3000);
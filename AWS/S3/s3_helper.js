/*
 * AWS Setup
 */
var AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';
var s3 = new AWS.S3();


/**
 * @param {string} bucketName
 * @param {string} fileName 
 */
exports.getItem = function(bucketName, fileName){
    //fileName = encodeURIComponent(fileName);
    return new Promise(function (resolve, reject){

		var params = {
            Bucket: bucketName,
            Key: fileName
        }
        
        s3.getObject(params, function(err, data) {
            if (err){
                reject(err);
            }
            else{
                var data = data.Body.toString('utf-8');
                resolve(data);
            }
        });
        
	});	
}


/**
 * @param {string} bucketName
 * @param {string} fileName 
 * @param {string} data 
 */
exports.createItem = function(bucketName, fileName, data){
    return this.createItemWithMetadata(bucketName, fileName, {}, data);
}


/**
 * @param {string} bucketName
 * @param {string} fileName 
 * @param {string} data 
 * @param {Object} metadata
 */
exports.createItemWithMetadata = function(bucketName, fileName, metadata, data){
    return new Promise(function (resolve, reject){

		var params = {
            Bucket: bucketName,
            Key: fileName,
            Body: data,
            Metadata: metadata
        }

        s3.putObject(params, function(err, data) {
            if (err){
                reject(err);
            }
            else{
                resolve(data);
            }
        });
        
	});	
}

/**
 * @param {string} bucketName
 * @param {string} fileName 
 * @param {Object} metadata
 * You can set object metadata at the time you upload it. 
 * After you upload the object, you cannot modify object metadata.
 */
exports.updateMetadata = function(bucketName, fileName, metadata){
    return new Promise(function (resolve, reject){
		var params = {
            Bucket: bucketName,
            CopySource: bucketName + "/" + fileName,
            Key: fileName,
            Metadata: metadata,
            MetadataDirective: 'REPLACE'
        }

        s3.copyObject(params, function(err, data) {
            if (err){
                reject(err);
            }
            else{
                resolve(data);
            }
        });        
	});	
}
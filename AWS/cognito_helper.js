const aws = require('aws-sdk');
var cognitoidentityserviceprovider = new aws.CognitoIdentityServiceProvider();

/**
 * @param {object} params
 */
exports.createUser = function(params){
    return new Promise(function(resolve, reject){
          cognitoidentityserviceprovider.adminCreateUser(params, function(err1, data1) {
            if (err1) {
                console.log(err1, err1.stack); // an error occurred
                reject(err1);
            }
            else  {  
                //console.log(data1);           // successful response
                resolve(data1);
            }
        });
    });
}
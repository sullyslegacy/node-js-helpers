/**
 *  Create an index
 *  @param {string} hostName
 *  @param {string} indexName
 */
exports.createIndex = function(hostName, indexName){
    var url = hostName + indexName + "?pretty";
	//Create
}


/**
 *  Delete an index
 *  @param {string} hostName
 *  @param {string} indexName
 */
exports.deleteIndex = function(hostName, indexName){
    var url = hostName + indexName + "?pretty";
	//Delete
}


/**
 *  Create document in index and type
 *  @param {string} hostName
 *  @param {string} indexName
 *  @param {string} typeName
 *  @param {object} document
 */
exports.createDocument = function(hostName, indexName, typeName, document){
    var url = hostName + indexName + typeName;
	
}
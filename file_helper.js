var fs = require('fs');

/**
 * Reads file from system
 * @param {string} data
 */
exports.readFile = function(fileName){
    return new Promise(function (resolve, reject){
       fs.readFile(fileName, 'utf8', function (err, data) {
            if (err) {
                reject(err);
            }
            else{
                resolve(data);
            }
        });
    });
}


/**
 * Reads file from system synch
 * @param {string} data
 */
exports.readFileSynch = function(fileName){
    return fs.readFileSync(fileName, 'utf8');
}


/**
 * Writes file to system
 * @param {string} data
 */
exports.writeFile = function(fileName, data){
    return new Promise(function (resolve, reject){
       fs.writeFile(fileName, data, function (err) {
            if (err) {
                reject(err);
            }
            else{
                resolve(true);
            }
        });
    });
}


/**
 * Gets all files at path
 * @param {string} path
 */
exports.getFiles = function(path){
    return new Promise(function (resolve, reject){
        fs.readdir(path, 'utf8', function (err, files) {
            if (err) {
                reject(err);
            }
            else{
                resolve(files);
            }
        });
    });
}



function readAndSaveImage(){
	var fs = require('fs');
	var image = fs.readFileSync('pirate.JPG', 'binary');
	var file = fs.createWriteStream('pirate2.JPG', 'binary');
	file.write(image);
	file.close();
	file.end();


	/*
	var s3Helper = require('./helpers/s3_helper');
	var fs = require('fs');

	//Read image as binary file
	var image = fs.readFileSync('pirate.JPG', 'binary');

	//Make sure image in binary file before we pass to S3
	image = new Buffer(image, 'binary');

	//Save to S3
	s3Helper.createItem("omsg-server-config", "pirate2.JPG", image);
	*/	
}
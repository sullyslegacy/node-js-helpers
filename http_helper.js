var http = require('http');
var request = require('request');


function httpCallback(resolve, reject, error, response, body) {
	if(error){
		var statusCode = response && response.statusCode;
		var errorResponse = {
			"success": false,
			"statusCode": statusCode,
			"error": error
		};
		reject(errorResponse);
		return;
	}
	const contentType = response.headers['content-type'];
	if(contentType == 'application/json'){
		try{
            body = JSON.parse(body);
        }
        catch(error){
            console.log("Error httpCallback can't parse JSON");
        }
	}
	resolve(body);
}

function createHttpRequest(httpUrl, methodType, data){
	var httpRequest = {
		url: httpUrl,
		method: methodType,
		headers: {
			"content-type": "application/json",
			},
		//json: data
		body: data
	};
	return httpRequest;
}

function createHttpFormRequest(httpUrl, methodType, data){
	var httpRequest = {
		url: httpUrl,
		method: methodType,
		formData: data
	};
	return httpRequest;
}

/**
 * 	HTTP GET Request
 *  @param {string} httpUrl
 */
exports.httpGet = function(httpUrl){
	return new Promise(function (resolve, reject){
		request.get(httpUrl, function (error, response, body){
			httpCallback(resolve, reject, error, response, body);
		});
	});
}

/**
 * 	HTTP POST Request
 *  @param {string} httpUrl
 *  @param {string} data
 */
exports.httpPost = function(httpUrl, data){
	return new Promise(function (resolve, reject){
		var httpRequest = createHttpRequest(httpUrl, "POST", data);
		request.post(httpRequest, function (error, response, body){
			httpCallback(resolve, reject, error, response, body);
		});
	});
}

/**
 * 	HTTP PUT Request
 *  @param {string} httpUrl
 *  @param {string} data
 */
exports.httpPut = function(httpUrl, data){
	return new Promise(function (resolve, reject){
		var httpRequest = createHttpRequest(httpUrl, "PUT", data);
		request.put(httpRequest, function (error, response, body){
			httpCallback(resolve, reject, error, response, body);
		});
	});
}

/**
 * 	HTTP DELETE Request
 *  @param {string} httpUrl
 *  @param {string} data
 */
exports.httpDelete = function(httpUrl, data){
	return new Promise(function (resolve, reject){
		var httpRequest = createHttpRequest(httpUrl, "DELETE", data);
		request.delete(httpRequest, function (error, response, body){
			httpCallback(resolve, reject, error, response, body);
		});
	});
}

/**
 * 	HTTP Form POST Request
 *  @param {string} httpUrl
 *  @param {string} data
 */
exports.httpFormPost = function(httpUrl, data){
	return new Promise(function (resolve, reject){
		var httpRequest = createHttpFormRequest(httpUrl, "POST", data);
		request.post(httpRequest, function (error, response, body){
			httpCallback(resolve, reject, error, response, body);
		});
	});
}

/**
 * 	HTTP GET Buffer Request
 *  Useful when getting a File
 *  @param {string} httpUrl
 */
exports.httpGetBuffer = function(httpUrl){
	return new Promise(function (resolve, reject){
		/*
		 * Set encoding to null, so body is returned as a Buffer
		 * httpResponse contains body
		 */
		request.get({url: httpUrl, encoding: null}, function optionalCallback(err, httpResponse, body) {
			if (err) {
				reject(err);
			}			
			resolve(body);
		});
	});
}

/**
 * 	HTTP Form Post Buffer Request
 *  Useful when getting a File
 *  @param {string} httpUrl
 */
exports.httpFormPostBuffer = function(httpUrl, data){
	return new Promise(function (resolve, reject){
		request.post({url: httpUrl, formData: data, encoding: null}, function optionalCallback(err, httpResponse, body) {
			if (err) {
				reject(err);
			}			
			resolve(body);
		});
	});
}



/**
 * 	HTTP GET Binary Request
 *  Useful when getting a File
 *  @param {string} httpUrl
 */
exports.httpGetBufferOld = function(httpUrl){
	return new Promise(function (resolve, reject){
		http.get(httpUrl, (res) => {
			const { statusCode } = res;
			const contentType = res.headers['content-type'];
			
			if (statusCode !== 200) {
				let error = new Error('Request Failed. Status Code:' + statusCode);
				reject(error.message);
				// consume response data to free up memory
				res.resume();
				return;
			}
			
			var rawData = []; 
			res.on('data', (chunk) => { rawData.push(chunk); });
			res.on('end', () => {
				try {
					rawData = Buffer.concat(rawData); 
					resolve(rawData);
				} 
				catch (error) {
					console.log(error);
					reject(error.message);
				}
			});
		}).on('error', (error) => {
			reject(error.message);
		});
	});
}
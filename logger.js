var log4js = require('log4js'); 
log4js.loadAppender('file');

log4js.addAppender(log4js.appenders.file('logs/debug.log'), 'Debug');
var debugLogger = log4js.getLogger('Debug');
debugLogger.setLevel('DEBUG');

log4js.addAppender(log4js.appenders.file('logs/info.log'), 'Info');
var infoLogger = log4js.getLogger('Info');
infoLogger.setLevel('INFO');

log4js.addAppender(log4js.appenders.file('logs/error.log'), 'Error');
var errorLogger = log4js.getLogger('Error');
errorLogger.setLevel('ERROR');

/**
 * @param {string} message
 */
exports.logDebug = function(message){
    errorLogger.debug(message);
}

/**
 * @param {string} message
 */
exports.logInfo = function(message){
    infoLogger.info(message);
}

/**
 * @param {string} message
 */
exports.logError = function(message){
    errorLogger.error(message);
}
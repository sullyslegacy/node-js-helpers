/**
 * Pass in an array of promises to be called sequenctially
 * @param {array} promises
 */
exports.executePromiseFunctionsSequenctially = function(promises){
    return new Promise(function (resolve, reject){
        var promiseRequest = {
            "promises": promises,
            "responses": []
        };

        executePromiseFunctionsSequenctiallyRecursive(promiseRequest).then((dataResponse) => {
            resolve(promiseRequest.responses);
        });    
    });
}

function executePromiseFunctionsSequenctiallyRecursive(promiseRequest){
    var promises = promiseRequest.promises;
    
    return new Promise(function (resolve, reject){
	    if(promises.length == 0){
            resolve(promiseRequest);
            return promiseRequest;
        }
        
        var currentElement = promises.splice(0, 1)[0];
        currentElement().then(function(response) {
            promiseRequest.responses.push(response);
            resolve( executePromiseFunctionsSequenctiallyRecursive(promiseRequest) );
        }).catch(function(error){
            console.log('ERROR executePromiseFunctionsSequenctiallyRecursive: ' + JSON.stringify(error) );
            reject(error);
        });
    });
}


/**
 * Pass in an array of promises, where x promises will run at once then we will run the next x promises
 * @param {array} promises
 * @param {int} promisesToRunAtOnce
 */
exports.executePromisesInSegments = function(promises, promisesToRunAtOnce){
    var promiseSegments = [];
    var promiseSegment = [];
    for(var i = 0; i < promises.length; i++){
        promiseSegment.push(promises[i]);
        if(i != 0 && (i + 1) % promisesToRunAtOnce == 0){
            promiseSegments.push(promiseSegment);
            promiseSegment = [];
        }
    }
    if(promiseSegment.length > 0){
        promiseSegments.push(promiseSegment);
    }

    return new Promise(function (resolve, reject){
        var promiseRequest = {
            "promises": promiseSegments,
            "responses": []
        };
        executePromiseSegmentRecursively(promiseRequest).then((data) => {
            var responses = [];
            for(var i = 0; i < data.responses.length; i++){
                responses = responses.concat(data.responses[i]);
            }
            resolve(responses);
        }).catch(function(error){
            console.log('ERROR executePromisesInSegments: ' + JSON.stringify(error) );
            reject(error);
        });
    });
}

function executePromiseSegmentRecursively(promiseRequest){
    var promises = promiseRequest.promises;
    
    return new Promise(function (resolve, reject){
	    if(promises.length == 0){
            resolve(promiseRequest);
            return promiseRequest;
        }

        var promiseArray = [];
        var currentSegment = promises.splice(0, 1)[0];
        for(var i = 0; i < currentSegment.length; i++){
            promiseArray.push(currentSegment[i]());
        }
        Promise.all(promiseArray).then(data => { 
            promiseRequest.responses.push(data);
            resolve( executePromiseSegmentRecursively(promiseRequest) );
        }).catch(function(error){
            console.log('ERROR executePromiseSegmentRecursively: ' + JSON.stringify(error) );
            reject(error);
        });
    });
}


/**
 * Always try to run X promises, when one promise finishes launch next promise
 * @param {array} promises
 * @param {int} promisesToRunAtOnce
 */
exports.executeXPromisesConcurrently = function(promises, promisesToRunAtOnce){
    return new Promise(function (resolve, reject){
        if(promisesToRunAtOnce >= promises.length){
            Promise.all(promises).then(data => { 
                resolve(data);
            }).catch(function(error){
                console.log('ERROR executeXPromisesConcurrently: ' + JSON.stringify(error) );
                reject(error);
            });
        }
        else{
            var promiseRequest = {
                "promises": promises,
                "responses": [],
                "size": promises.length
            };
            
            for(var i = 0; i < promisesToRunAtOnce; i++){
                executeXPromisesConcurrentlyRecursive(promiseRequest).then((dataResponse) => {
                    resolve(promiseRequest.responses);
                });        
            }
        }
    });
}

function executeXPromisesConcurrentlyRecursive(promiseRequest){
    var promises = promiseRequest.promises;
    
    return new Promise(function (resolve, reject){
	    if(promises.length == 0){
            if(promiseRequest.responses.length == promiseRequest.size){
                resolve(promiseRequest);
            }
            return promiseRequest;
        }
        
        var currentElement = promises.splice(0, 1)[0];
        currentElement().then(function(response) {
            promiseRequest.responses.push(response);
            resolve( executeXPromisesConcurrentlyRecursive(promiseRequest) );
        }).catch(function(error){
            console.log('ERROR executeXPromisesConcurrentlyRecursive: ' + JSON.stringify(error) );
            reject(error);
        });
    });
}
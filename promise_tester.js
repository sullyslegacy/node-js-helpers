var promiseHelper = require('./promise_helper');
const runs = 20;
const parallelRuns = 5;
const milliTime = 1000;

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

function display(text, sleepTime){
    return new Promise(function (resolve, reject){
        if(text == "i=2" || text == "i=14" || text == "i=18"){
            sleepTime = 10000;
        }
        else{
            sleepTime = 100;
        }
        sleep(sleepTime).then(() => {
            resolve(text);
        });        
    });
}

var myFunction = function(text, sleepTime){
    return function(){
        return Promise.resolve( display(text, sleepTime) );
    };
};

function createPromiseArray(){
    var promiseArray = [];
    for(var i = 0; i < runs; i++){
        promiseArray.push(myFunction('i=' + i, i * milliTime));
    }
    return promiseArray;
}

function log(text){
    console.log(new Date() + " " + text);
}

function main(){
    log("Start");
    var promiseArray = createPromiseArray();
    
    return new Promise(function (resolve, reject){
        //promiseHelper.executePromiseFunctionsSequenctially(promiseArray).then((data) => {
        //promiseHelper.executePromisesInSegments(promiseArray, parallelRuns).then((data) => {
        promiseHelper.executeXPromisesConcurrently(promiseArray, parallelRuns).then((data) => {
            console.log(JSON.stringify(data));
            log("Finish");
            //resolve(true);
        }).catch(function(error){
            console.log('ERROR main: ' + JSON.stringify(error) );
            //reject(utility.createErrorMessage(error));
        });
    });
}

main();
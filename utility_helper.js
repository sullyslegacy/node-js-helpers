/**
 *  Remove blank fields in JS Object
 *  @param {object} data
 */
exports.removeEmptyFields = function(data){
	for(key in data){
		if(data[key] == ""){
			delete data[key];
		}
	}
	return data;
}

/**
 *  Create Error Message
 *  @param {object, string} errorObject
 */
exports.createErrorMessage = function(errorObject){
	var errorData = {
		"success": false,
		"error": errorObject
	}
	return errorData;
}

/**
 *  Takes a map that contains arrays, and makes array elements key
 *  @param {object} myMap
 */
exports.reverseMapArray = function(myMap){
    var newMap = {};
    for(key in myMap){
        var elements = myMap[key];
        for(var i = 0; i < elements.length; i++){
            var element = elements[i];
            newMap[element] = key;             
        }
    }
    return newMap;
}
